<?php
$db = new PDO("sqlite:fonoteka.db");
// Обработка POST-запроса при нажатии кнопки "Добавить"
if (isset($_POST['add'])) {
    $year = $_POST['year'];
    $artist = $_POST['artist'];
    $album = $_POST['album'];

    // Вставка данных в базу данных
    $sql = "INSERT INTO fon (year, artist, album) VALUES (:year, :artist, :album)";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':year', $year);
    $stmt->bindParam(':artist', $artist);
    $stmt->bindParam(':album', $album);

    if ($stmt->execute()) {
        echo "Данные успешно добавлены в базу данных.";
        }
    else {
        echo "Ошибка при добавлении данных: " . $stmt->errorInfo()[2];
    }
}
?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<head>
    <title>Album List</title>
    <style>
        label, input[type="text"], input[type="number"] {
            display: inline-block;
            vertical-align: middle;
        }

        label {
            width: 80px; /* Ширина метки */
        }
        .error {
            border-color: red;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: #f2f2f2;
        }

        .sortable {
            cursor: pointer;
        }
        .asc::after {
            content: " ▲";
            color: black;
        }

        .desc::after {
            content: " ▼";
            color: black;
        }


    </style>
</head>
<body>
<h1>Album List</h1>

<form id="albumForm" method="POST">
    <div class="form-row">
        <div class="form-group col-md-2">
            <label for="year">Год:</label>
            <input type="number" class="form-control" id="year" name="year">
        </div>

        <div class="form-group col-md-4">
            <label for="artist">Артист:</label>
            <input type="text" class="form-control" id="artist" name="artist">
        </div>

        <div class="form-group col-md-4">
            <label for="album">Альбом:</label>
            <input type="text" class="form-control" id="album" name="album">
        </div>

        <div class="form-group col-md-2">
            <button type="submit"   class="btn btn-primary addButton">Добавить</button>
        </div>
    </div>
</form>

<table id="albumTable">
    <thead>
    <tr>
        <th class="sortable" data-column="id" >ID</th>
        <th class="sortable" data-column="year">Year</th>
        <th class="sortable" data-column="artist">Artist</th>
        <th class="sortable" data-column="album">Album</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $db = new PDO("sqlite:fonoteka.db");
    $sql = "SELECT * FROM fon";
    $result = $db->query($sql);
    foreach ($result as $row) {
        echo "<tr data-id='" . $row['id'] . "'>";
        echo "<td class='id'>" . $row['id'] . "</td>";
        echo "<td class='year'>" . $row['year'] . "</td>";
        echo "<td class='artist'>" . $row['artist'] . "</td>";
        echo "<td class='album'>" . $row['album'] . "</td>";
        echo "<td>
        <form method='POST'>
            <button class='editButton' type='submit' value='" . $row['id'] . "' id='editButton'>Изменить</button>
       </form>   
        <form method='POST' action='task11.php'>
            <button class='deleteButton' name='delete' type='submit' value='" . $row['id'] . "'>Удалить</button>
        </form> 
    </td>";
    }
    $db = null;
    ?>
    </tbody>
</table>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        function isValidYear(year) {
            // Проверяем, что год состоит из 4 цифр
            if (year.length !== 4 || isNaN(year)) {
                return false;
            }
            // Проверяем, что год находится в диапазоне от 1900 до текущего года
            var currentYear = new Date().getFullYear();
            if (year < 1900 || year > currentYear) {
                return false;
            }

            return true;
        }

        // Обработчик события нажатия кнопки "Изменить"
        $(".editButton").click(function(event) {
            event.preventDefault(); // Предотвращаем отправку формы

            // Проверка текста кнопки
            var buttonText = $(this).text();
            if (buttonText === "Изменить") {
                var row = $(this).closest("tr");
                var lastYear = row.find(".year").text();
                var lastArtist = row.find(".artist").text();
                var lastAlbum = row.find(".album").text();
                $(this).text("Сохранить");
                row.find(".year").html("<input type='number' id='newYear' class='form-control' value='" + lastYear + "'>");
                row.find(".artist").html("<input type='text' id='newArtist' class='form-control' value='" + lastArtist + "'>");
                row.find(".album").html("<input type='text' id='newAlbum' class='form-control' value='" + lastAlbum + "'>");
                return; // Прерываем выполнение функции, чтобы не делать AJAX запрос
            }
            else {
                var editId = $(this).val();
                var editForm = $("#albumTable").find("tr[data-id='" + $(this).val() + "']");
                var year = editForm.find("#newYear").val();
                var artist = editForm.find("#newArtist").val();
                var album = editForm.find("#newAlbum").val();
            }

            // Проверка корректности поля "year"
            if (!isValidYear(year)) {
                editForm.find("#newYear").addClass('error');
                return; // Прерываем выполнение функции, чтобы форма не была отправлена
            }else {
                editForm.find("#newYear").removeClass('error');
            }
            if (artist.trim() === "") {
                editForm.find("#newArtist").addClass('error');
                return;
            } else {
                editForm.find("#newArtist").removeClass('error');
            }
            if (album.trim() === "") {
                editForm.find("#newAlbum").addClass('error');
                return;
            } else {
                editForm.find("#newAlbum").removeClass('error');
            }

            // Создание объекта данных для отправки на сервер
            var data = {
                edit: 'edit',
                edit_id: editId,
                newYear: year,
                newArtist: artist,
                newAlbum: album
            };

            // Отправка AJAX-запроса
            $.ajax({
                method: "POST",
                url: "task1.php",
                data: data,
                success: function(response) {
                    var updatedData = JSON.parse(response);
                    // Обновление данных в таблице
                    var row = $("#albumTable").find("tr[data-id='" + updatedData.id + "']");
                    row.find(".year").text(updatedData.year);
                    row.find(".artist").text(updatedData.artist);
                    row.find(".album").text(updatedData.album);
                    $(".editButton").text("Изменить");
                },
                error: function(xhr, status, error) {
                    // Обработка ошибки
                    $(".editButton").text("Изменить");
                }
            });
        });

        $(".deleteButton").click(function(event) {
            event.preventDefault(); // Предотвращаем отправку формы
            var deleteId = $(this).val();
            // Создание объекта данных для отправки на сервер
            var data = {
                delete: deleteId,
            };

            // Отправка AJAX-запроса
            $.ajax({
                method: "POST",
                url: "task1.php",
                data: data,
                success: function(response) {
                    var deleteData = JSON.parse(response);
                    var row = $("#albumTable").find("tr[data-id='" + deleteData.id + "']");
                    row.remove()
                },
                error: function(xhr, status, error) {
                    // Обработка ошибки

                }
            });
        });

        $(".addButton").click(function(event) {
            event.preventDefault(); // Предотвращаем отправку формы

            // Получение значений полей формы
            var editForm = $("#albumForm");
            var year = editForm.find("#year").val();
            var artist = editForm.find("#artist").val();
            var album = editForm.find("#album").val();

            // Проверка корректности поля "year"
            console.log(editForm.find("#year"));
            if (!isValidYear(year)) {
                editForm.find("#year").addClass('error');
                return; // Прерываем выполнение функции, чтобы форма не была отправлена
            }else {
                editForm.find("#year").removeClass('error');
            }
            if (artist.trim() === "") {
                editForm.find("#artist").addClass('error');
                return;
            } else {
                editForm.find("#artist").removeClass('error');
            }
            if (album.trim() === "") {
                editForm.find("#album").addClass('error');
                return;
            } else {
                editForm.find("#album").removeClass('error');
            }
            // Создание объекта данных для отправки на сервер
            var data = {
                add: 'add',
                year: year,
                artist: artist,
                album: album
            };

            // Отправка AJAX-запроса
            $.ajax({
                method: "POST",
                url: "task11.php",
                data: data,
                success: function(response) {
                    $("#albumTable").html(response);

                    // Clear form fields
                    editForm.find("#year").val('');
                    editForm.find("#artist").val('');
                    editForm.find("#album").val('');
                },
                error: function(xhr, status, error) {
                    // Обработка ошибки

                }
            });
        });

        $(".sortable").click(function() {
            // Получение значения атрибута data-column
            var column = $(this).data("column");

            // Получение текущего порядка сортировки
            var currentOrder = $(this).hasClass("asc") ? "asc" : "desc";

            // Изменение порядка сортировки
            var newOrder = currentOrder === "asc" ? "desc" : "asc";

            // Удаление классов сортировки у всех заголовков колонок
            $(".sortable").removeClass("asc desc");

            // Добавление класса сортировки к текущему заголовку колонки
            $(this).addClass(newOrder);

            // Вызов функции для сортировки таблицы
            sortTable(column, newOrder);
        });

// Функция для сортировки таблицы
        function sortTable(column, order) {
            var table = $("#albumTable");
            var rows = table.find("tbody tr").get();

            rows.sort(function(a, b) {
                var aValue = $(a).find("." + column).text();
                var bValue = $(b).find("." + column).text();

                if (order === "asc") {
                    return aValue.localeCompare(bValue);
                } else {
                    return bValue.localeCompare(aValue);
                }
            });

            $.each(rows, function(index, row) {
                table.children("tbody").append(row);
            });
        }

    });
</script>
</body>
</html>