<?php


$db = new PDO("sqlite:fonoteka.db");
// Обработка POST-запроса при нажатии кнопки "Добавить"


// Обработка POST-запроса при нажатии кнопки "Удалить"
if (isset($_POST['delete'])) {
    $id = $_POST['delete'];
    // Удаление данных из базы данных
    $sql = "DELETE FROM fon WHERE id = :id";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $id);
    if ($stmt->execute()) {
        $deleteData = [
            'id' => $id,
        ];
        echo json_encode($deleteData);
    } else {
        echo "Ошибка при удалении данных: " . $stmt->errorInfo()[2];
    }
}

if (isset($_POST['edit'])) {
    $edit_id = $_POST['edit_id'];
    $newYear = $_POST['newYear'];
    $newArtist = $_POST['newArtist'];
    $newAlbum = $_POST['newAlbum'];

    // Обновление данных в базе данных
    $sql = "UPDATE fon SET year = :year, artist = :artist, album = :album WHERE id = :id";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':year', $newYear);
    $stmt->bindParam(':artist', $newArtist);
    $stmt->bindParam(':album', $newAlbum);
    $stmt->bindParam(':id', $edit_id);

    if ($stmt->execute()) {
        $updatedData = [
            'id' => $edit_id,
            'year' => $newYear,
            'artist' => $newArtist,
            'album' => $newAlbum
        ];
        echo json_encode($updatedData);
    }
    else {
        echo "Ошибка при обновлении данных: " . $stmt->errorInfo()[2];
    }
}

$db = null; // Закрытие соединения с базой данных
?>